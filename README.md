# Google Sheets Table

This module provides a new field type called "Google sheets table" which
accepts a Google sheet spreadsheet ID and converts it into an HTML table. 
This HTML is stored in the database and can be rendered like a text field,
exported to a decoupled app or used however else you like!

Additionally, a cron task is provided that automatically checks if
the linked Google sheets have any new revisions and updates the respective HTML
tables in the database.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/google_sheets_table).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/google_sheets_table).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Key](https://www.drupal.org/project/key)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Follow [Google's instructions to create a user-managed Service account](https://cloud.google.com/iam/docs/service-accounts) with read access.
1. Create a key for this Service account (choose JSON) and download the JSON file.
1. Store the JSON file as a multi-value key using the [Key module](https://www.drupal.org/project/key).
1. On your Drupal site, go to `/admin/config/services/google-sheets-table` and
choose the Key you just created as the Google sheets API key. 

Now the module can view any Google sheets that the Service account has access to. Note
that this module does not require write permissions and will only ever read 
data from Google sheets.

To use this feature, create a new field of type "Google sheets table" and configure it on the respective entity. Then create or edit an entity with the field and insert the "spreadsheet ID" of a Google spreadsheet into the Google sheets table field. The field will be populated with HTML table markup imported from the spreadsheet, and automatically updated on cron.

### Spreadsheet ID

The spreadsheet ID is the randomized string of characters in the url of the spreadsheet after `/spreadsheets/d/`. For example, this
[spreadsheet](https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit#gid=0)
has an ID of `1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms`.
See the official [Google Sheets API](https://developers.google.com/sheets/api/guides/concepts)
for more information on their terminology.


## Maintainers

- Jay Huskins - [jayhuskins](https://www.drupal.org/u/jayhuskins)
