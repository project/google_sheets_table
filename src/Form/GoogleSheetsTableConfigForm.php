<?php

namespace Drupal\google_sheets_table\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\google_sheets_table\GoogleSheetsApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form builder for the Google sheets table configuration form.
 *
 * @package Drupal\google_sheets_table\Form
 */
class GoogleSheetsTableConfigForm extends ConfigFormBase {

  /**
   * Google Sheets API service.
   *
   * @var \Drupal\google_sheets_table\GoogleSheetsApi
   */
  protected $googleSheetsApi;

  /**
   * Constructs a new GoogleSheetsTableConfigForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\google_sheets_table\GoogleSheetsApi $google_sheets_api
   *   The Google Sheets API service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, GoogleSheetsApi $google_sheets_api) {
    parent::__construct($config_factory);
    $this->googleSheetsApi = $google_sheets_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('google_sheets_api'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'google_sheets_table.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_sheets_table_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('google_sheets_table.settings');

    $form['credentials'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Secret key'),
      '#required' => TRUE,
      '#description' => $this->t('Also, the key has to belong to the <strong>multi-value authentication</strong> group'),
      '#key_filters' => ['type' => 'authentication_multivalue'],
      '#default_value' => $config->get('credentials') ?? '',
    ];

    $form = parent::buildForm($form, $form_state);
    $form['actions']['connection'] = [
      '#type' => 'submit',
      '#value' => $this->t('Test connection'),
      '#submit' => ['::testConnectionSubmit'],
    ];

    return $form;
  }

  /**
   * Submission handler to test the connection.
   */
  public function testConnectionSubmit(array &$form, FormStateInterface $form_state) {
    $credentials = [];
    $credential_key = $form_state->getValue('credentials');

    // Make a test call to make sure that the credentials are correct.
    try {
      $this->googleSheetsApi->createClient($credential_key);
      $this->messenger()->addStatus($this->t('The test connection to Google Sheets services was successful.'));
    }
    catch (\Exception $exception) {
      $this->messenger()->addError($this->t('The Google Sheets test connection failed with the message <em>@message</em>.', [
        '@message' => $exception->getMessage(),
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    // Save the configuration.
    $this->config('google_sheets_table.settings')
      ->set('credentials', $form_state->getValue('credentials'))
      ->save();
  }

}
