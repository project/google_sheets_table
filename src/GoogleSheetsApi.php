<?php

namespace Drupal\google_sheets_table;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\key\KeyInterface;
use Drupal\key\KeyRepositoryInterface;
use Google\Client;
use Google\Service\Drive;
use Google\Service\Sheets;

/**
 * Google sheets API service.
 *
 * @package Drupal\google_sheets_table
 */
class GoogleSheetsApi implements GoogleSheetsApiInterface {

  /**
   * Google API client.
   *
   * @var \Google\Client
   */
  protected $client;

  /**
   * Google Sheets service.
   *
   * @var \Google\Service\Sheets
   */
  protected $sheets;

  /**
   * Google Drive service.
   *
   * @var \Google\Service\Drive
   */
  protected $drive;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;


  /**
   * The cache service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The key repository.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * GoogleSheetsApi constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache service.
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   The key repository service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
    CacheBackendInterface $cache,
    KeyRepositoryInterface $key_repository
  ) {
    $this->configFactory = $config_factory;
    $this->loggerFactory = $logger_factory;
    $this->cache = $cache;
    $this->keyRepository = $key_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function createClient($credential_key = NULL) {
    if (empty($credential_key)) {
      $settings = $this->configFactory->get('google_sheets_table.settings');
      if (!empty($settings)) {
        $credential_key = $settings->get('credentials');
      }
    }

    if (empty($credential_key)) {
      $error = 'No credential key configured';
      $this->loggerFactory->get('google_sheets_table')->error($error);
      throw new \Exception($error);
    }

    $key = $this->keyRepository->getKey($credential_key);
    if ($key instanceof KeyInterface && !empty($credentials = $key->getKeyValue())) {
      $credentials = json_decode($credentials, TRUE);
      $this->client = new Client();
      $this->client->setAuthConfig($credentials);
      $this->client->setApplicationName('Drupal Google Sheets API');
      $this->client->setScopes([
        Sheets::DRIVE_READONLY,
        Sheets::SPREADSHEETS_READONLY,
      ]);
    }
    else {
      $error = 'Invalid credential key configured';
      $this->loggerFactory->get('google_sheets_table')->error($error);
      throw new \Exception($error);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getService() {
    if (!$this->sheets || !$this->drive) {
      try {
        if (!$this->client) {
          $this->createClient();
        }
        $this->sheets = new Sheets($this->client);
        $this->drive = new Drive($this->client);
      }
      catch (\Exception $e) {
        $this->loggerFactory->get('google_sheets_table')->error('Unable to establish Google sheets API service');
        return FALSE;
      }
    }

    return $this->sheets;
  }

  /**
   * {@inheritdoc}
   */
  public function getSheets(string $spreadsheet_id) {
    $this->getService();

    try {
      $spreadsheet = $this->sheets->spreadsheets->get($spreadsheet_id, [
        'includeGridData' => TRUE,
      ]);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('google_sheets_table')->error('Unable to access spreadsheet with ID ' . $spreadsheet_id);
      return FALSE;
    }

    return $spreadsheet->getSheets() ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getSpreadsheetLastModified($spreadsheet_id) {
    $this->getService();

    try {
      $file = $this->drive->files->get($spreadsheet_id, [
        'supportsAllDrives' => TRUE,
        'fields' => 'modifiedTime',
      ]);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('google_sheets_table')->error('Unable to access file with ID ' . $spreadsheet_id);
      return FALSE;
    }

    $last_modified = $file->getModifiedTime();

    $timestamp = (new \DateTime($last_modified))->getTimestamp();

    return $timestamp;
  }

}
