<?php

namespace Drupal\google_sheets_table;

/**
 * Provides Google sheets API service.
 *
 * @package Drupal\google_sheets_table
 */
interface GoogleSheetsApiInterface {

  /**
   * Creates an authorized Google API client.
   */
  public function createClient();

  /**
   * Returns Google sheets service.
   *
   * @return \Google\Service\Sheets
   *   The Google sheets service.
   */
  public function getService();

  /**
   * Get the Google sheets from the spreadsheet.
   *
   * @param string $spreadsheet_id
   *   The spreadsheet ID.
   *
   * @return \Google\Service\Sheets\Sheet[]
   *   The sheets for the given spreadsheet.
   */
  public function getSheets(string $spreadsheet_id);

  /**
   * Get the timestamp for when the spreadsheet was last modified.
   *
   * @param string $spreadsheet_id
   *   The spreadsheet ID.
   *
   * @return int
   *   The unix timestamp the spreadsheet was last modified.
   */
  public function getSpreadsheetLastModified(string $spreadsheet_id);

}
