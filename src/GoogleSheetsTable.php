<?php

namespace Drupal\google_sheets_table;

use Drupal\Core\TypedData\TypedData;

/**
 * Save the table data from a Google spreadsheet ID as HTML.
 */
class GoogleSheetsTable extends TypedData {

  /**
   * The value of the Google Sheets Table.
   *
   * @var string
   */
  protected $value;

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    if ($value) {
      $this->value = $value;
    }
    else {
      // Get field data.
      $item = $this->getParent();
      $spreadsheet_id = $item->spreadsheet_id;
      $value = '';

      if ($spreadsheet_id) {
        // Use Google sheets API to fetch sheets.
        $api = $this->getGoogleSheetsApi();
        $last_modified = $api->getSpreadsheetLastModified($spreadsheet_id);
        if ($last_modified) {

          $sheets = $api->getSheets($spreadsheet_id);
          if ($sheets !== FALSE) {
            // On successful access to spreadsheet, update last sync time.
            $item->last_sync_time = \Drupal::time()->getRequestTime();
            $converter = $this->getGoogleSheetsTableConverter();
            foreach ($sheets as $sheet) {
              $value .= $converter->convertSheetToHtml($sheet, $item->row_header, $item->col_header);
            }
          }
        }
      }
      $this->value = $value;
    }

    // Notify the parent of any changes.
    if ($notify && isset($this->parent)) {
      $this->parent->onChange($this->name);
    }
  }

  /**
   * Returns the Google sheets API service.
   *
   * @return \Drupal\google_sheets_table\GoogleSheetsApi
   *   The Google sheets API service
   */
  protected function getGoogleSheetsApi() {
    return \Drupal::service('google_sheets_api');
  }

  /**
   * Returns the Google sheets table converter service.
   *
   * @return \Drupal\google_sheets_table\GoogleSheetsTableConverter
   *   The Google sheets API service
   */
  protected function getGoogleSheetsTableConverter() {
    return \Drupal::service('google_sheets_table_converter');
  }

}
