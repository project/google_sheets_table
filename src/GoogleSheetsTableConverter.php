<?php

namespace Drupal\google_sheets_table;

use Google\Service\Sheets\CellData;
use Google\Service\Sheets\Sheet;

/**
 * Google sheets table converter service.
 *
 * @package Drupal\google_sheets_table
 */
class GoogleSheetsTableConverter implements GoogleSheetsTableConverterInterface {

  /**
   * {@inheritdoc}
   */
  public function createTag($name, $content = '', array $attributes = []) {
    if ($name === 'script') {
      return '';
    }
    $attribute_string = ' ';
    if (!empty($attributes)) {
      foreach ($attributes as $key => $value) {
        if (is_int($key)) {
          continue;
        }
        $attribute_string .= $key . '="' . $value . '" ';
      }
    }
    return '<' . $name . $attribute_string . '>' . $content . '</' . $name . '>';
  }

  /**
   * {@inheritdoc}
   */
  public function convertSheetToHtml(Sheet $sheet, bool $row_header = FALSE, bool $col_header = FALSE) {
    $data = $sheet->getData();
    if (empty($data)) {
      return '';
    }
    $table_rows = $data[0]->getRowData();

    $row_spans = [];
    $col_spans = [];
    foreach ($sheet->getMerges() as $merge) {
      $column_distance = $merge->endColumnIndex - $merge->startColumnIndex;
      $row_distance = $merge->endRowIndex - $merge->startRowIndex;
      // Check distance between columns to check if it's a rowspan or colspan.
      if ($column_distance > 1) {
        $col_spans[] = [
          'colspan' => $column_distance,
          'start_col' => $merge->startColumnIndex,
          'end_col' => $merge->endColumnIndex,
          'row' => $merge->startRowIndex,
        ];
      }
      elseif ($row_distance > 1) {
        $row_spans[] = [
          'rowspan' => $row_distance,
          'start_row' => $merge->startRowIndex,
          'end_row' => $merge->endRowIndex,
          'column' => $merge->startColumnIndex,
        ];
      }
    }

    $html_rows = '';
    $consecutive_empty_rows = 0;
    foreach ($table_rows as $row_index => $row) {
      $html_row = '';
      $row_content = FALSE;
      if (!empty($row)) {
        $row_element = 'td';
        if ($row_index === 0 && $row_header) {
          $row_element = 'th';
        }

        $cells = $row->getValues();
        foreach ($cells as $col_index => $cell) {
          $inside_span = FALSE;
          $attributes = [];
          $cell_element = $row_element;
          if ($col_index === 0 && $col_header) {
            $cell_element = 'th';
          }
          $cell_content = $this->formatCellContent($cell);
          if (!$row_content && $cell_content) {
            $row_content = TRUE;
          }

          foreach ($col_spans as $col_span) {
            if ($row_index == $col_span['row']) {
              if ($col_index == $col_span['start_col']) {
                $attributes['colspan'] = $col_span['colspan'];
                break;
              }
              elseif (!$cell_content && $col_index > $col_span['start_col'] && $col_index < $col_span['end_col']) {
                $inside_span = TRUE;
                break;
              }
            }
          }
          foreach ($row_spans as $row_span) {
            if ($col_index == $row_span['column']) {
              if ($row_index == $row_span['start_row']) {
                $attributes['rowspan'] = $row_span['rowspan'];
                break;
              }
              elseif (!$cell_content && $row_index > $row_span['start_row'] && $row_index < $row_span['end_row']) {
                $inside_span = TRUE;
                break;
              }
            }
          }

          if (!$inside_span) {
            $html_row .= $this->createTag($cell_element, $cell_content, $attributes);
          }
        }
      }

      if (!$row_content) {
        $consecutive_empty_rows++;
        if ($consecutive_empty_rows > 2) {
          break;
        }
      }
      else {
        $consecutive_empty_rows = 0;
      }
      $html_rows .= $this->createTag('tr', $html_row);
    }

    return $this->createTag('table', $html_rows);
  }

  /**
   * {@inheritdoc}
   */
  public function formatCellContent(CellData $cell) {
    $cell_content = $cell->getFormattedValue();
    $text_format_runs = $cell->getTextFormatRuns();
    if (!empty($text_format_runs)) {
      $formatted_cell_content = '';
      // Walk backwards through TextFormatRuns
      // so we can do basic text formatting.
      $text_format_runs = array_reverse($text_format_runs);
      $last_position = NULL;
      $pieces = [];
      foreach ($text_format_runs as $index => $text_format_run) {
        $start = $text_format_run->startIndex ?? 0;
        $format = $text_format_run->getFormat();

        if ($last_position) {
          $text = substr($cell_content, $start, $last_position - $start);
        }
        else {
          $text = substr($cell_content, $start);
        }

        // @todo Make format mapping configurable.
        if ($format->italic) {
          $text = $this->createTag('em', $text);
        }
        if ($format->bold) {
          $text = $this->createTag('b', $text);
        }

        $link = NULL;
        if ($format->link) {
          $link = $format->link->uri;
        }

        // Push to start of array so they are stored in original order.
        array_unshift($pieces, [
          'text' => $text,
          'link' => $link,
        ]);

        $last_position = $start;
      }
      $connected_link = '';
      $connected_link_text = '';
      // Walk through formatted pieces and connect shared links.
      foreach ($pieces as $piece) {
        if ($connected_link) {
          if (!$piece['link'] || $piece['link'] !== $connected_link) {
            $formatted_cell_content .= $this->createTag('a', $connected_link_text, ['href' => $connected_link]);
            $connected_link = $piece['link'] ?? '';
            $connected_link_text = $piece['text'] ?? '';
          }
          elseif ($piece['link'] === $connected_link) {
            $connected_link_text .= $piece['text'];
          }
        }
        elseif ($piece['link']) {
          $connected_link = $piece['link'];
          $connected_link_text = $piece['text'];
        }
        else {
          $formatted_cell_content .= $piece['text'];
        }
      }
      $cell_content = $formatted_cell_content;
    }

    if ($cell->effectiveFormat && $cell_format = $cell->effectiveFormat->textFormat) {
      // @todo Make cell format mapping configurable.
      // Italic.
      if ($cell_format->italic) {
        $cell_content = $this->createTag('em', $cell_content);
      }
      // Bold.
      if ($cell_format->bold) {
        $cell_content = $this->createTag('b', $cell_content);
      }
    }
    // Link.
    if ($hyperlink = $cell->getHyperlink()) {
      $cell_content = $this->createTag('a', $cell_content, ['href' => $hyperlink]);
    }
    return $cell_content;
  }

}
