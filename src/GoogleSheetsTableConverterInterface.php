<?php

namespace Drupal\google_sheets_table;

use Google\Service\Sheets\CellData;
use Google\Service\Sheets\Sheet;

/**
 * Provides Google sheets table converter service.
 *
 * @package Drupal\google_sheets_table
 */
interface GoogleSheetsTableConverterInterface {

  /**
   * Builds an HTML tag given a name and content.
   *
   * @param string $name
   *   The name of the HTML tag as written in code.
   *   examples: "div", "p", "a".
   * @param string $content
   *   The contents of the HTML tag.
   * @param array $attributes
   *   Key/value pairs of tag attributes.
   *
   * @return string
   *   HTML string for the requested tag.
   */
  public function createTag($name, $content = '', array $attributes = []);

  /**
   * Convert Google sheet to HTML table.
   *
   * @param Google\Service\Sheets\Sheet $sheet
   *   The Google sheet.
   * @param bool $row_header
   *   If TRUE, format the first row as a table header.
   * @param bool $col_header
   *   If TRUE, format the first column as a table header.
   *
   * @return string
   *   HTML table string.
   */
  public function convertSheetToHtml(Sheet $sheet, bool $row_header = FALSE, bool $col_header = FALSE);

  /**
   * Convert TextFormatRuns to HTML.
   *
   * @param Google\Service\Sheets\CellData $cell
   *   The cell data object.
   *
   * @return string
   *   Cell content HTML.
   */
  public function formatCellContent(CellData $cell);

}
