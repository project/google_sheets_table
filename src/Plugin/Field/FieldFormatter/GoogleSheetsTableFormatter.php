<?php

namespace Drupal\google_sheets_table\Plugin\Field\FieldFormatter;

use Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter;

/**
 * Extend 'text_default' formatter to allow Google sheets table fields.
 *
 * @FieldFormatter(
 *   id = "google_sheet_table_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "google_sheets_table_field"
 *   }
 * )
 */
class GoogleSheetsTableFormatter extends TextDefaultFormatter {

}
