<?php

namespace Drupal\google_sheets_table\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\text\Plugin\Field\FieldType\TextLongItem;

/**
 * Stores an HTML table imported from Google sheets.
 *
 * @FieldType(
 *  id = "google_sheets_table_field",
 *  label = @Translation("Google sheets table"),
 *  description = @Translation("This field stores an HTML table imported from Google sheets."),
 *  category = @Translation("Text"),
 *  default_widget = "google_sheets_table_widget",
 *  default_formatter = "google_sheet_table_default"
 * )
 */
class GoogleSheetsTableItem extends TextLongItem {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['spreadsheet_id'] = [
      'type' => 'varchar_ascii',
      'length' => 128,
    ];
    $schema['columns']['value'] = [
      'type' => 'text',
      'size' => 'big',
    ];
    $boolean_definition = [
      'type' => 'int',
      'size' => 'tiny',
    ];
    $schema['columns']['col_header'] = $boolean_definition;
    $schema['columns']['row_header'] = $boolean_definition;
    $schema['columns']['last_sync_time'] = [
      'type' => 'int',
    ];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Get "processed" and "format" properties from TextLongItem.
    $properties = parent::propertyDefinitions($field_definition);

    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Table'))
      ->setRequired(FALSE)
      ->setComputed(TRUE)
      ->setClass('Drupal\google_sheets_table\GoogleSheetsTable');

    $properties['spreadsheet_id'] = DataDefinition::create('string')
      ->setLabel(t('Spreadsheet ID'))
      ->setRequired(TRUE);

    $properties['row_header'] = DataDefinition::create('boolean')
      ->setLabel(t('Header row'));

    $properties['col_header'] = DataDefinition::create('boolean')
      ->setLabel(t('Header column'));

    $properties['last_sync_time'] = DataDefinition::create('timestamp')
      ->setLabel(t('Last sync time'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $spreadsheet_id = $this->get('spreadsheet_id')->getValue();

    return $spreadsheet_id === NULL || $spreadsheet_id === '';
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    $entity = $this->getEntity();
    if (!$entity->isNew() && $this->value && $this->spreadsheet_id) {
      // Get previous values.
      $original = $entity->original;
      $langcode = $entity
        ->language()
        ->getId();
      if ($original && $original->hasTranslation($langcode)) {
        $original_values = $original->getTranslation($langcode)->get($this->getFieldDefinition()->getName());
        // Check for changes in spreadsheet_id, row_header, and col_header.
        if ($original_values->spreadsheet_id !== $this->spreadsheet_id ||
          $original_values->row_header != $this->row_header ||
          $original_values->col_header != $this->col_header) {
          // Clear table so it will re-import with updated values.
          $this->value = '';
        }
      }
    }
  }

}
