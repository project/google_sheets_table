<?php

namespace Drupal\google_sheets_table\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'google_sheets_table' widget.
 *
 * @FieldWidget(
 *  id = "google_sheets_table_widget",
 *  label = @Translation("Google sheets table"),
 *  field_types = {
 *    "google_sheets_table_field"
 *  },
 * )
 */
class GoogleSheetsTableWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['fieldset'] = $element + [
      '#type' => 'fieldset',
    ];
    $element['fieldset']['spreadsheet_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Spreadsheet ID'),
      '#default_value' => $items[$delta]->spreadsheet_id ?? NULL,
      '#size' => 60,
      '#placeholder' => '',
      '#maxlength' => $this->getFieldSetting('max_length'),
      '#attributes' => ['class' => ['js-text-full', 'text-full']],
    ];

    $element['fieldset']['row_header'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Header row'),
      '#default_value' => !empty($items[$delta]->row_header),
    ];

    $element['fieldset']['col_header'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Header column'),
      '#default_value' => !empty($items[$delta]->col_header),
    ];
    $element['format'] = [
      '#type' => 'text_format',
      '#base_type' => 'value',
      '#default_value' => $items[$delta]->value ?? NULL,
      '#format' => $items[$delta]->format ?? NULL,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    return array_map(function ($value) {
      return $value['format'] + $value['fieldset'];
    }, $values);
  }

}
