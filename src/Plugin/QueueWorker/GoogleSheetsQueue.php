<?php

namespace Drupal\google_sheets_table\Plugin\QueueWorker;

use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\google_sheets_table\GoogleSheetsApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes cron tasks to update Google sheets tables.
 *
 * @QueueWorker(
 *   id = "google_sheets_table_update_queue",
 *   title = @Translation("Google sheets table update queue"),
 *   cron = {"time" = 10}
 * )
 */
class GoogleSheetsQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The Google sheets API.
   *
   * @var \Drupal\google_sheets_table\GoogleSheetsApi
   */
  protected $api;

  /**
   * Constructs a new GoogleSheetsQueue instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   * @param \Drupal\google_sheets_table\GoogleSheetsApi $api
   *   The Google sheets API service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database, GoogleSheetsApi $api) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
    $this->api = $api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      \Drupal::service('google_sheets_api'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $entity_key = $data['entity_key'];
    $fields = $data['fields'];

    // Skip item if missing data or entity_key is malformed.
    if (empty($entity_key) || !strstr($entity_key, ':') || empty($fields)) {
      return;
    }

    $entity_parts = explode(':', $entity_key);
    $entity_type = $entity_parts[0];
    $entity_id = $entity_parts[1];

    $storage = \Drupal::service('entity_type.manager')->getStorage($entity_type);
    if (empty($storage)) {
      return;
    }

    $entity = $storage->load($entity_id);
    if (empty($entity)) {
      return;
    }

    $entity_updated = FALSE;

    foreach ($fields as $field) {
      if (!$entity->hasField($field)) {
        continue;
      }
      $field_items = $entity->{$field}->getValue();
      foreach ($field_items as $delta => $field_item) {
        // Fetch the Google sheet's last modified time.
        $last_modified_time = $this->api->getSpreadsheetLastModified($field_item['spreadsheet_id']);
        $last_sync_time = $field_item['last_sync_time'];

        // If the sheet has been updated since our last sync,
        // then clear the table value and re-import.
        if ($last_modified_time && (!$last_sync_time || $last_sync_time < $last_modified_time)) {
          $field_item['value'] = '';
          $entity->{$field}->set($delta, $field_item);
          $entity_updated = TRUE;
        }
      }
    }

    if ($entity_updated) {
      $entity->save();
    }
  }

}
