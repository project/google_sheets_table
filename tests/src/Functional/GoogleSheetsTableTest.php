<?php

namespace Drupal\Tests\google_sheets_table\Functional;

use Drupal\Tests\BrowserTestBase;
use Google\Service\Sheets\Sheet;

/**
 * Tests for the Google Sheets Table module.
 *
 * @group google_sheets_table
 */
class GoogleSheetsTableTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['key', 'google_sheets_table'];

  /**
   * A user with relevant administrative privileges.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Perform initial setup tasks that run before every test method.
   */
  public function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer google_sheets_table',
    ]);
  }

  /**
   * Tests the configuration form.
   */
  public function testConfigForm() {
    $this->drupalLogin($this->adminUser);

    // Access configuration page.
    $this->drupalGet('admin/config/services/google-sheets-table');
    $this->assertSession()->statusCodeEquals(200);

    // Credentials field exists.
    $this->assertSession()->selectExists('credentials');
  }

  /**
   * Test the GoogleSheetsTableConverter.
   */
  public function testTableConverter() {
    $converter = \Drupal::service('google_sheets_table_converter');
    $sheet = $this->getTestSheet();
    $configurations = [
      'no_headers' => [
        'th_num' => 0,
        'row_header' => FALSE,
        'col_header' => FALSE,
      ],
      'row_header' => [
        'th_num' => 4,
        'row_header' => TRUE,
        'col_header' => FALSE,
      ],
      'col_header' => [
        'th_num' => 7,
        'row_header' => FALSE,
        'col_header' => TRUE,
      ],
      'both' => [
        'th_num' => 10,
        'row_header' => TRUE,
        'col_header' => TRUE,
      ],
    ];

    foreach ($configurations as $key => $config) {
      $html = $converter->convertSheetToHtml($sheet, $config['row_header'], $config['col_header']);
      $dom = new \DOMDocument();
      $dom->loadHTML($html);

      $this->assertEquals(
        12,
        $dom->getElementsByTagName('tr')->length,
        '12 rows found in the rendered HTML.'
      );
      $this->assertEquals(
        $config['th_num'],
        $dom->getElementsByTagName('th')->length,
      );
    }

  }

  /**
   * Load the test sheet JSON into a Google sheet object.
   *
   * @return \Google\Service\Sheets\Sheet
   *   The test sheet object.
   */
  protected function getTestSheet() {
    $module_path = \Drupal::service('extension.list.module')->getPath('google_sheets_table');
    $sheet_data_path = $module_path . '/tests/sheet.json';
    $sheet_data_file = file_get_contents(($sheet_data_path));
    $sheet_data = json_decode($sheet_data_file, TRUE);
    $sheet = new Sheet($sheet_data);

    return $sheet;
  }

}
